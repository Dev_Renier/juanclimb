<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'default',
            'email' => 'renier.trenuela@gmail.com',
            'type' => 'su',
            'password' => bcrypt('info1234'),
        ]);
    }
}
