<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function scopeFilter($query)
    {
        return $query->where('type', 'moderator');
    }

    public function new($request)
    {
        $user           = new $this;
        $user->name     = $request['name'];
        $user->email    = $request['email'];
        $user->password = bcrypt($request['password_confirmation']);
        $user->type     = $request['account_type'];
        $user->save();

        return 'success';
    }

    public function edit($request)
    {
        $model        = $this->find($request['id']);
        $model->name  = $request['name'];
        $model->email = $request['email'];
        $model->type  = $request['account_type'];
        $model->save();

        return 'success';
    }

    public function updatePassword($id, $newpass)
    {
        $user           = $this->find($id);
        $user->password = bcrypt($newpass);
        $user->save();

        return 'success';
    }

    public function search($q)
    {
        if(is_numeric($q))
        {
            return $this->query('id', $q);
        }
        return $this->query('name','LIKE','%'.$q.'%');
    }
}
