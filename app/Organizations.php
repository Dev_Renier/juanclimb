<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizations extends Model
{
    public $table = 'organization';

    /**
     * @param $request
     * @return string
     */
    public function store($request)
    {
        $model       = new $this;
        $model->name = $request['name'];
        $model->save();

        return 'success';
    }

    /**
     * @param $request
     * @return string
     */
    public function edit($request)
    {
        $model = $this->find($request['id']);
        $model->name = $request['name'];
        $model->save();

        return 'success';
    }
}
