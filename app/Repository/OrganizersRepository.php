<?php
namespace App\Repository;

use DB;

class OrganizersRepository
{
    public $organizers;

    public function __construct()
    {
        $this->organizers = DB::table('organizers');
    }

    public function getOrgsByMod()
    {
        return $this->organizers
                 ->select('organization.name', 'organization.id')
                 ->leftjoin('organization', 'organization.id', '=', 'organizers.organization_id')
                 ->where('organizers.user_id', auth()->user()->id)->get()->toArray();
    }
}