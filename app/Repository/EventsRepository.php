<?php

namespace App\Repository;

use DB;
use Carbon\Carbon;

class EventsRepository
{
    public $events;

    public function __construct()
    {
        $this->events = DB::table('event');
    }

    public function getByOrgID($id)
    {
        return $this->events
            ->selectRaw("event.id,
                    users.name, 
                    organization.name AS org_name, 
                    event.name AS event_name, 
                    event.date")
            ->join('organization', 'organization.id', '=', 'event.organization_id')
            ->join('users', 'users.id', '=', 'event.user_id')
            ->where('event.organization_id', $id);
    }

    public function getOrgByEvent($id)
    {
        return $this->events
                   ->selectRaw("organization.name AS org_name, event.name AS event_name, event.*")
                   ->join('organization', 'organization.id', '=', 'event.organization_id')
                   ->where('event.id', $id)->get()->toArray()[0];
    }

    public function updateEvent($request)
    {
        $this->events
            ->where('event.id', $request['event_id'])
            ->update([
                'name'            => $request['name'],
                'place'           => $request['place'],
                'description'     => $request['description'],
                'price'           => $request['price'],
                'date'            => Carbon::parse($request['date'])->format('Y-m-d'),
                'organization_id' => $request['org_id'],
                'image'           => $request['file'],
            ]);

        return 'success';
    }
}