<?php
namespace App\Repository;

use DB;

class OrganizationsRepository
{
    public $organizations;

    public function __construct()
    {
        $this->organizations = DB::table('organization');
    }

    public function getOrgName($id)
    {
        return $this->organizations
                   ->select('name')
                   ->where('id', $id)
                   ->get()->toArray()[0]->name;
    }
}