<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPass extends Mailable
{
    use Queueable, SerializesModels;

    protected $request;

    /**
     * Create a new message instance.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(User $user)
    {
        $newpass = str_random(8);
        $user->updatePassword($this->request['id'], $newpass);

        $this->view('mailables.resetpass')
        ->with([
            'newpass' => $newpass
        ]);

        return $newpass;
    }
}
