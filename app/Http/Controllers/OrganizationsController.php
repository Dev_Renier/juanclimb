<?php

namespace App\Http\Controllers;

use App\Organizations;
use App\Organizers;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OrganizationsController extends Controller
{
    public $organizations;

    /**
     * OrganizationsController constructor.
     *
     * @param Organizations $organizations
     */
    public function __construct(Organizations $organizations)
    {
        $this->organizations = $organizations;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.organizations');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return DataTables::of($this->organizations->all())->make(true);
    }

    /**
     * @param Request $request
     * @return string
     * @internal param Organizations $organizations
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        return $this->organizations->store($request->input());
    }

    /**
     * @param Request $request
     * @return mixed
     * @internal param Organizations $organizations
     */
    public function show(Request $request)
    {
        return $this->organizations->find($request->input())->toArray()[0];
    }

    /**
     * @param Request $request
     * @return string
     * @internal param Organizations $organizations
     */
    public function edit(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        return $this->organizations->edit($request->input());
    }

    /**
     * @param Request    $request
     * @param Organizers $organizers
     * @return int
     * @internal param Organizations $organizations
     */
    public function delete(Request $request, Organizers $organizers)
    {
        $result = $organizers->where('organization_id', $request->get('id'))->get()->toArray();
        if (count($result) != 0) {
            return 'xxx';
        }

        return $this->organizations->destroy($request->get('id'));
    }
}
