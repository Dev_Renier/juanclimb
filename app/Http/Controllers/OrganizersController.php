<?php

namespace App\Http\Controllers;

use App\Organizations;
use App\Organizers;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class OrganizersController extends Controller
{
    protected $organizers;

    /**
     * OrganizersController constructor.
     *
     * @param Organizers $organizers
     */
    public function __construct(Organizers $organizers)
    {
        $this->organizers = $organizers;
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return DataTables::of($this->organizers->all())->make(true);
    }

    public function addModerator(Request $request)
    {
        $request->validate([
            'mod' => 'required',
        ]);

        $result = $this->organizers->where('user_id', $request->mod)
                                   ->where('organization_id', $request->org)
                                   ->count();

        if ($result > 0) {
            return 'exist';
        }
        $oragnizers                  = new Organizers;
        $oragnizers->user_id         = $request->mod;
        $oragnizers->organization_id = $request->org;
        $oragnizers->save();

        return 'success';
    }

    public function getModerator(Request $request)
    {
        return $this->organizers->where('organization_id', $request->get('id'))->with('user')->get();
    }

    public function removeModerator(Request $request)
    {
        $this->organizers->destroy($request->get('id'));

        return 'success';
    }
}
