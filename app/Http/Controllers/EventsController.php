<?php

namespace App\Http\Controllers;

use App\Events;
use App\Repository\EventsRepository;
use App\Repository\OrganizationsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repository\OrganizersRepository;
use DB;
use Yajra\DataTables\DataTables;

class EventsController extends Controller
{
    public $organizersRepository;

    public $organizationsRepository;

    public $eventsRepository;

    /**
     * EventsController constructor.
     *
     * @internal param OrganizersRepository $organizersRepository
     * @internal param OrganizationsRepository $organizationsRepository
     */
    public function __construct()
    {
        $this->organizersRepository    = new OrganizersRepository();
        $this->organizationsRepository = new OrganizationsRepository();
        $this->eventsRepository        = new EventsRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orgs = $this->organizersRepository->getOrgsByMod();

        return view('admin.events', compact('orgs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $org_name = $this->organizationsRepository->getOrgName($request->id);
        $id       = $request->id;

        return view('admin.eventsadd', compact('org_name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     */
    public function store(Request $request)
    {
        $request->validate([
            'date'        => "required",
            'description' => "required",
            'name'        => "required",
            'price'       => "required|numeric",
        ]);

        $event                  = new Events();
        $event->date            = Carbon::parse($request->get('date'))->format('Y-m-d');
        $event->name            = $request->get('name');
        $event->price           = $request->get('price');
        $event->description     = $request->get('description');
        $event->place           = $request->get('place');
        $event->status          = '0';
        $event->user_id         = auth()->user()->id;
        $event->organization_id = $request->get('org_id');
        $event->image           = $request->get('file');
        $event->save();

        return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = $this->eventsRepository->getOrgByEvent($id);

        return view('admin.eventsedit', compact('event', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string
     * @internal param int $id
     */
    public function update(Request $request)
    {
        return $this->eventsRepository->updateEvent($request->input());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return string
     * @internal param int $id
     */
    public function destroy(Request $request)
    {
        DB::table('event')->where('id', $request->get('id'))->delete();

        return 'success';
    }

    /**
     * Retrieve event list by org_id.
     *
     * @param Request    $request
     * @param DataTables $dataTables
     * @return \Illuminate\Http\JsonResponse
     */
    public function retrieve(Request $request, DataTables $dataTables)
    {
        $events = $this->eventsRepository->getByOrgID($request->get('id'));

        return $dataTables->queryBuilder($events)->make(true);
    }
}
