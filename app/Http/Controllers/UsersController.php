<?php

namespace App\Http\Controllers;

use App\Mail\ResetPass;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(User $user)
    {
        return view('admin.users');
    }

    /**
     * @return mixed
     */
    public function table()
    {
        return DataTables::of(User::all())->make(true);
    }

    /**
     * @param Request $request
     * @param User    $user
     * @return string
     */
    public function register(Request $request, User $user)
    {
        $request->validate([
            'name'         => 'required',
            'email'        => 'required|email|unique:users',
            'password'     => 'required|confirmed',
            'account_type' => 'required',
        ]);

        return $user->new($request->input());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getById(Request $request)
    {
        return User::find($request->id)->toArray();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function searchUser(Request $request)
    {
        return User::find($request->id)->toArray();
    }

    /**
     * @param Request $request
     * @param User    $user
     * @return string
     */
    public function update(Request $request, User $user)
    {
        return $user->edit($request->input());
    }

    /**
     * @param Request $request
     * @param User    $user
     * @return int
     */
    public function delete(Request $request, User $user)
    {
        return $user->destroy($request->get('id'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function resetPassword(Request $request)
    {
        return Mail::to(User::find($request->get('id'))->email)->send(new ResetPass($request->input()));
    }

    /**
     * @param Request $request
     * @param User    $user
     * @return string
     */
    public function updatePassword(Request $request, User $user)
    {
        $request->validate([
            'password_old' => 'required',
            'password'     => 'required|confirmed',
        ]);

        if (Auth::attempt(['id' => auth()->user()->id, 'password' => $request->get('password_old')])) {
            $user->updatePassword(auth()->user()->id, $request->get('password'));
        }

        return 'success';
    }

    public function getUser(Request $request)
    {
        return User::where('name','LIKE','%'.$request->get('q').'%')->get();
    }
}
