<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizers extends Model
{
    protected $table = 'organizers';

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
