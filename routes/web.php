<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/only', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/only', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::prefix('users')->group(function () {
        Route::get('/', 'UsersController@index')->name('users');
        Route::any('register', 'UsersController@register')->name('users.register');
        Route::get('table', 'UsersController@table')->name('users.table');
        Route::get('{id}/get', 'UsersController@getById')->name('users.get');
        Route::get('get', 'UsersController@getUser')->name('users.get.it');
        Route::get('update', 'UsersController@update')->name('users.update');
        Route::get('delete', 'UsersController@delete')->name('users.delete');
        Route::get('new/pass', 'UsersController@resetPassword')->name('users.reset.password');
        Route::get('update/pass', 'UsersController@updatePassword')->name('users.update.password');
    });

    Route::prefix('org')->group(function () {
        Route::get('/', 'OrganizationsController@index')->name('organizations');
        Route::get('table', 'OrganizationsController@table')->name('organizations.table');
        Route::get('add', 'OrganizationsController@store')->name('organizations.store');
        Route::get('update', 'OrganizationsController@edit')->name('organizations.edit');
        Route::get('get', 'OrganizationsController@show')->name('organizations.show');
        Route::get('delete', 'OrganizationsController@delete')->name('organizations.delete');
    });

    Route::prefix('organizers')->group(function () {
        Route::get('table', 'OrganizersController@table')->name('organizers.table');
        Route::get('add/mod', 'OrganizersController@addModerator')->name('organizers.add');
        Route::get('get/mod', 'OrganizersController@getModerator')->name('organizers.get');
        Route::get('delete/mod', 'OrganizersController@removeModerator')->name('organizers.remove');
    });

    Route::prefix('events')->group(function () {
        Route::get('/', 'EventsController@index')->name('events');
        Route::get('/add/{id}', 'EventsController@create')->name('events.create');
        Route::get('/{id}/edit', 'EventsController@edit')->name('events.edit');
        Route::get('/insert', 'EventsController@store')->name('events.store');
        Route::post('/update', 'EventsController@update')->name('events.update');
        Route::get('/delete', 'EventsController@destroy')->name('events.destroy');
        Route::get('/get', 'EventsController@retrieve')->name('events.retrieve');
    });

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
});
