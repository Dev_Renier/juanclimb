@extends('layouts.admin')

@section('content')
    <div id="panel" class="panel panel-default">
        <div class="panel-heading">Organizations</div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button v-on:click="showMdlOrg" class="btn btn-success">Add Organization</button>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <table class="table" id="org-table"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.addorganizations')
    @include('admin.partials.updateorganizations')
    @include('admin.partials.assignorganizers')
    @include('admin.partials.updateorganizer')
@endsection

@section('scripts')
    <script>
        var app = {
            el: '#app',
            data: {
                dt: null,
                reg_org: {
                    id: '',
                    name: ''
                },
                update_org: {
                    id: '',
                    name: ''
                },
                selected: {
                    mod: '',
                    org: ''
                },
                selected_view: '',
                moderators: {}
            },
            methods: {
                showMdlOrg: function () {
                    $('#addOrgMdl').modal('show');
                    this.reg_org = {id: '', name: ''};
                },
                addOrg: function () {
                    var $this = this;
                    $.ajax({
                        url: 'org/add',
                        method: 'GET',
                        data: $this.reg_org,
                        success: function (data) {
                            $this.dt.draw();
                            swal('New Org has been added', '', 'success');
                            $('#addOrgMdl').modal('hide');
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                showUpdateMdlOrg: function (id) {
                    var $this = this;
                    $this.update_org.id = id;
                    $('#updateOrgMdl').modal('show');
                    $.ajax({
                        url: 'org/get',
                        method: 'GET',
                        data: $this.update_org,
                        success: function (data) {
                            $this.update_org.name = data.name;
                        }
                    });
                },
                editOrg: function () {
                    var $this = this;
                    $.ajax({
                        url: 'org/update',
                        method: 'GET',
                        data: $this.update_org,
                        success: function (data) {
                            $this.dt.draw();
                            swal('Org Updated!', '', 'success');
                            $('#updateOrgMdl').modal('hide');
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                addToMod: function () {
                    var $this = this;
                    $.ajax({
                        url: 'organizers/add/mod',
                        method: 'GET',
                        data: $this.selected,
                        success: function (data) {
                            if(data == 'success') {
                                $this.dt.draw();
                                swal('Moderator has been added!', '', 'success');
                                $('#updateOrgMdl').modal('hide');
                            } else {
                                swal('Moderator already exist!', '', 'warning');
                            }
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                showOrganizers: function (id) {
                    var $this = this;
                    $this.selected_view = id;
                    $('#updateOrganizerMdl').modal('show');
                    $.ajax({
                        url: 'organizers/get/mod',
                        method: 'GET',
                        data: {'id': id},
                        success: function (data) {
                            $this.moderators = data;
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                deleteOrganizer: function (id) {
                    var $this = this;
                    $.ajax({
                        url: 'organizers/delete/mod',
                        method: 'GET',
                        data: { 'id': id },
                        success: function (data) {
                            swal('Moderator has been deleted!', '', 'success');
                            $this.showOrganizers($this.selected_view);
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                }
            },
            mounted: function () {
                var $this = this;
                $this.dt = $('#org-table').DataTable({
                    serverSide: true,
                    processing: true,
                    scrollX: true,
                    dom: 'Blrtip',
                    ajax: 'org/table',
                    columns: [
                        {data: 'id', name: 'id', title: 'ID'},
                        {data: 'name', name: 'name', title: 'Name'},
                        {
                            data: function (data) {
                                var BtnView = '<button data-id="' + data.id + '" class="btn btn-default btn-xs showModMdl">View</button>';
                                return BtnView;
                            }, name: 'id', title: 'Moderators'
                        },
                        {
                            data: function (data) {
                                var BtnEdit = '<button data-id="' + data.id + '" class="btn btn-info btn-xs showUpdateMdlOrg">Edit</button>';
                                var BtndDelete = '<button data-id="' + data.id + '" class="btn btn-danger btn-xs delOrg">Delete</button>';
                                var BtndAssign = '<button data-id="' + data.id + '" class="btn btn-warning btn-xs assignOrg">Assign User</button>';
                                return BtnEdit + BtndDelete + BtndAssign;
                            }, name: 'id', title: 'Action'
                        },
                    ]
                });

                $('body').on('click', '.assignOrg', function () {
                    $this.selected.org = $(this).attr('data-id');
                    $('#assignOrganizersMdl').modal('show');
                });
                $('body').on('click', '.showUpdateMdlOrg', function () {
                    $this.showUpdateMdlOrg($(this).attr('data-id'));
                });
                $('body').on('click', '.delOrg', function () {
                    var id = $(this).attr('data-id');
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function () {
                        $.ajax({
                            url: 'org/delete',
                            method: 'GET',
                            data: {id: id},
                            success: function (data) {
                                if (data == 'xxx') {
                                    swal('Still has Moderators!', '', 'warning');
                                } else {
                                    swal('Org Deleted!', '', 'success');
                                }
                                $this.dt.draw();
                                $('#updateOrgMdl').modal('hide');
                            }
                        });
                    });
                });
                $('.user-list').select2({
                    ajax: {
                        url: 'users/get',
                        dataType: 'json',
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    },
                    width: '100%',
                    placeholder: '-- Select User --'
                }).on('change', function (e) {
                    $this.selected.mod = $(this).select2('data')[0].id;
                });

                $('body').on('click', '.showModMdl', function () {
                    $this.showOrganizers($(this).attr('data-id'));
                });

                $('body').on('click', '.deleteOrganizer', function () {
                    $this.deleteOrganizer($(this).attr('data-id'));
                });

            }
        };
    </script>
@endsection