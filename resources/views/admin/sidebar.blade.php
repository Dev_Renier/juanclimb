<div class="panel panel-default">
    <div class="panel-body">
        <ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
            <li role="presentation" class="{{ \Request::route()->getName() != 'dashboard'?:'active'  }}">
                <a href="/dashboard">Dashboard</a>
            </li>
            <li role="presentation" class="{{ \Request::route()->getName() != 'users'?:'active'  }}">
                <a href="/users">Users</a>
            </li>
            <li role="presentation" class="{{ \Request::route()->getName() != 'organizations'?:'active'  }}">
                <a href="/org">Origanizations</a>
            </li>
            <li role="presentation" class="{{ \Request::route()->getPrefix() != '/events'?:'active'  }}">
                <a href="/events">My Events</a>
            </li>
        </ul>
    </div>
</div>