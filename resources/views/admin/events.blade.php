@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header" data-background-color="purple">
            <h4 class="title">Events</h4>
            <p class="category">Here are your list of events.</p>
        </div>
        <div class="card-content">
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-2 col-md-offset-2">
                        <a href="#" class="btn btn-primary" @click="goToAddEvent"><i class="fa fa-star"></i> Add
                            Event</a>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label col-md-4">Events Filtered by: </label>
                        <div class="col-md-7">
                            <select class="form-control" @change="filterByOrg" v-model="org_selected">
                                <option value=""> -- Select your Organization --</option>
                                @foreach($orgs as $org)
                                    <option value="{{ $org->id }}">{{ $org->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <table class="table events-list"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var app = {
            el: '#app',
            data: {
                dt: null,
                org_selected: '',
                table_data: {}
            },
            methods: {
                goToAddEvent: function () {
                    if (this.org_selected > 0)
                        window.location.href = '/events/add/' + this.org_selected + '/';
                    else
                        swal('Please try again!', 'Choose your Organization', 'warning')
                },
                filterByOrg: function () {
                    var $this = this;
                    $this.dt.draw();
                }
            },
            mounted: function () {
                var $this = this;
                $this.dt = $('.events-list').DataTable({
                    serverSide: true,
                    processing: true,
                    scrollX: true,
                    dom: 'Blrtip',
                    order: [[1, 'desc']],
                    ajax: {
                        url: '/events/get',
                        data: function (d) {
                            d.id = $this.org_selected;
                        },
                    },
                    columns: [
                        {data: 'event_name', name: 'event_name', title: 'Event'},
                        {data: 'date', name: 'date', title: 'Date'},
                        {
                            data: function (data) {
                                var btn = '<a data-id="' + data.id + '" class="btn btn-flat btn-xs btn-info edit">' +
                                    '<i class="fa fa-pencil"></i></a>';
                                btn += '<a data-id="' + data.id + '" class="btn btn-flat btn-xs btn-danger delete">' +
                                    '<i class="fa fa-trash"></i></a>';
                                return '<div class="btn-group">' + btn + '</div>';
                            }, name: 'id', title: 'Action'
                        },
                    ],
                    drawCallback: function () {
                        $(document.body).on('click', '.edit', function () {
                            window.location.href = "/events/" + $(this).attr('data-id') + '/edit';
                        });
                        $(document.body).on('click', '.delete', function () {
                            var id = $(this).attr('data-id');
                            $.ajax({
                                url: '/events/delete',
                                data: {id: id},
                                success: function (data) {
                                    $this.dt.draw();
                                },
                                error: function (data) {
                                    var message = '';
                                    $.each(data.responseJSON.errors, function (x, y) {
                                        message += y[0] + '\n';
                                    });
                                    swal('Please try again', message, 'error');
                                }
                            });
                        });
                    }
                });
            }
        }
    </script>
@endsection