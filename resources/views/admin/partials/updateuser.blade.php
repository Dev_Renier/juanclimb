
<div class="modal fade" id="updateUserMdl" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update User</h5>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Name</label>
                        <div class="col-sm-6">
                            <input class="form-control" v-model="update_data.name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">E-mail</label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" v-model="update_data.email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Account Type</label>
                        <div class="col-sm-6">
                            <select class="form-control" v-model="update_data.account_type">
                                <option value="mod">Moderator</option>
                                @if(auth()->user()->type == 'su')
                                    <option value="su">Super User</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" v-on:click="updateUser">Save changes</button>
            </div>
        </div>
    </div>
</div>