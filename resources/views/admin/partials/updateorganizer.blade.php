
<div class="modal fade" id="updateOrganizerMdl" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Organizers</h5>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <li type="button" class="list-group-item" v-for="moderator in moderators">
                        @{{ moderator.user.name }}
                            <button data-id=""
                                    v-bind:data-id="moderator.id"
                                    class="btn btn-xs btn-danger deleteOrganizer">Delete</button>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" >Save changes</button>
            </div>
        </div>
    </div>
</div>