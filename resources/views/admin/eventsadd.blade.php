@extends('layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header" data-background-color="purple">
            <h4 class="title">Create Event for {{ $org_name }}</h4>
            <p class="category">Fill up blanks and save to create an event.</p>
        </div>
        <div class="card-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-2"><span class="fa fa-info-circle"></span></label>
                        <div class="col-md-9">
                            <input class="form-control" placeholder="What is the name of your event?"
                                   v-model="form_request.name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2"><span class="fa fa-globe"></span></label>
                        <div class="col-md-9">
                            <input class="form-control" placeholder="Where is your event take place?"
                                   v-model="form_request.place">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2"><span class="fa fa-picture-o"></span></label>
                        <div class="col-md-9">
                            <input type="file" placeholder="Where is your event take place?"
                                   @change="onFileChange"  accept=".jpg, .jpeg, .png" style="opacity: 1;position: inherit;top: 10px;">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-2"><span class="fa fa-hourglass"></span></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control input-date" placeholder="When it will be held?" 
                            v-model="form_request.date" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2"><span class="fa fa-money"></span></label>
                        <div class="col-md-9">
                            <input type="number" class="form-control" placeholder="What will be the cost?"
                                   v-model="form_request.price">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <textarea id="editor" v-model="form_request.description">
                        </textarea>
                    </div>
                    <div class="form-group">
                        <div class="col-md-1 col-md-offset-10">
                            <a class="btn btn-success" @click="save">SAVE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
    <script>
        var app = {
            el: '#app',
            data: {
                form_request: {
                    name: '',
                    place: '',
                    description: '',
                    price: '',
                    date: '',
                    org_id: {{ $id }},
                    file: ''
                }
            },
            methods: {
                save: function () {
                    var $this = this;
                    $.ajax({
                        url: '/events/insert',
                        data: $this.form_request,
                        success: function(data) {
                            if(data == 'success')
                            {
                                window.history.back();
                            }
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                onFileChange(e) {
                    var files = e.target.files || e.dataTransfer.files;
                    if (!files.length)
                    return;
                    this.createImage(files[0]);
                },
                createImage(file) {
                    var image = new Image();
                    var reader = new FileReader();
                    var vm = this;

                    reader.onload = (e) => {
                        vm.form_request.file = e.target.result;
                    };
                    reader.readAsDataURL(file);
                },  
            },
            mounted: function () {
                var $this = this;
                var editor = CKEDITOR.replace('editor');
                editor.config.resize_enabled = false;

                editor.on('change', function (e) {
                    $this.form_request.description = editor.getData();
                });

                $('.input-date').daterangepicker({
                        singleDatePicker: true,
                        showDropdowns: true,
                        autoUpdateInput: false
                    },
                    function (start, end, label) {
                        $this.form_request.date = start.format('YYYY-MM-DD').toString();
                    });
            }
        };
    </script>
@endsection