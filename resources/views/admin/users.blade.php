@extends('layouts.admin')

@section('content')
    <div id="panel" class="panel panel-default">
        <div class="panel-heading">Users</div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button v-on:click="addUser" class="btn btn-success">Add User</button>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <table class="table" id="users-table"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.partials.registeruser')
    @include('admin.partials.updateuser')

@endsection

@section('scripts')
    <script>
        var app = {
            el: '#app',
            methods: {
                addUser: function (event) {
                    $('#registerUserMdl').modal('show');
                    this.reg_data = {
                        name: '',
                        email: '',
                        password: '',
                        password_confirmation: '',
                        account_type: 'mod'
                    }
                },
                registerUser: function () {
                    var reg_data = this.reg_data;
                    var dt = this.dt;
                    $.ajax({
                        url: '{{ route('users.register') }}',
                        method: 'GET',
                        data: reg_data,
                        success: function (data) {
                            swal('Success', 'New user inserted.', 'success');
                            $('#registerUserMdl').modal('hide');
                            dt.draw();
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                bindUser: function (id) {
                    $('#updateUserMdl').modal('show');
                    var $this = this;
                    $.ajax({
                        url: 'users/' + id + '/get',
                        method: 'GET',
                        success: function (data) {
                            $this.update_data = {
                                id: data.id,
                                name: data.name,
                                email: data.email,
                                account_type: data.type
                            }
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
                updateUser: function () {
                    var $this = this;
                    var dt = this.dt;
                    $.ajax({
                        url: '/users/update',
                        method: 'GET',
                        data: $this.update_data,
                        success: function (data) {
                            swal('Success', 'User has been updated.', 'success');
                            $('#updateUserMdl').modal('hide');
                            dt.draw();
                        },
                        error: function (data) {
                            var message = '';
                            $.each(data.responseJSON.errors, function (x, y) {
                                message += y[0] + '\n';
                            });
                            swal('Please try again', message, 'error');
                        }
                    });
                },
            },
            data: {
                dt: null,
                reg_data: {
                    name: '',
                    email: '',
                    password: '',
                    confirm_password: '',
                    account_type: 'mod'
                },
                update_data: {
                    name: '',
                    email: '',
                    account_type: 'mod'
                }
            },
            mounted: function () {
                var $this = this;

                $this.dt = $('#users-table').DataTable({
                    serverSide: true,
                    processing: true,
                    scrollX: true,
                    dom: 'Blrtip',
                    ajax: 'users/table',
                    columns: [
                        {data: 'id', name: 'id', title: 'User ID'},
                        {data: 'name', name: 'name', title: 'Name'},
                        {data: 'email', name: 'email', title: 'E-mail'},
                        {data: 'type', name: 'type', title: 'Type'},
                        {
                            data: function (data) {
                                var BtnEdit = '<button data-id="' + data.id + '" class="btn btn-info btn-xs editUser">Edit</button>';
                                var BtndDelete = '<button data-id="' + data.id + '" class="btn btn-danger btn-xs delUser">Delete</button>';
                                var BtndReset = '<button data-id="' + data.id + '" class="btn btn-warning btn-xs resetUser">RP</button>';
                                return BtnEdit + BtndDelete + BtndReset;
                            }, name: 'id', title: 'Action'
                        },
                    ]
                });

                $('body').on('click', '.editUser', function () {
                    $this.bindUser($(this).attr('data-id'));
                });

                $('body').on('click', '.resetUser', function () {
                    var thisIs = $(this);
                    $.ajax({
                        url: '/users/new/pass',
                        method: 'GET',
                        data: {id: thisIs.attr('data-id')},
                        success: function (data) {
                            swal(
                                'New Password Sent!',
                                'Please see e-mail sent to this e-mail.',
                                'success'
                            );
                        },
                    });
                });

                $('body').on('click', '.delUser', function () {
                    var thisIs = $(this);
                    $.ajax({
                        url: '/users/new/pass',
                        method: 'GET',
                        data: {id: thisIs.attr('data-id')},
                        success: function (data) {
                            swal(
                                'Success!',
                                'New password has been sent in this email.',
                                'success'
                            );
                            $this.dt.draw();
                        },
                    });
                    swal({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: 'No, cancel!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function () {
                        $.ajax({
                            url: '/users/delete',
                            method: 'GET',
                            data: {id: thisIs.attr('data-id')},
                            success: function (data) {
                                swal(
                                    'Deleted!',
                                    'User has been deleted.',
                                    'success'
                                );
                                $this.dt.draw();
                            },
                        });
                    });
                });
            }
        };
    </script>
@endsection