<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/dasboard">
                    JUANCLIMB CONTROL PANEL
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="glyphicon glyphicon-cog"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" class="btnChangePass" @click="showChangePass">Change Password</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}">Log Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container admin-container">
        <div class="row">
            <div class="col-md-2">
                @include('admin.sidebar')
            </div>
            <div class="col-md-10">
                @yield('content')
            </div>
        </div>
    </div>

    <div id="changePassMdl" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Chnage Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-4">Old Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" v-model="new_pass.password_old">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">New Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" v-model="new_pass.password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Confirm New Password</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" v-model="new_pass.password_confirmation">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" @click="saveNewPass">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.10.2/sweetalert2.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.full.min.js"></script>
@yield('scripts')
<script>
    new Vue({
        mixins: [app],
        data: {
            new_pass: {
                password_confirmation: '',
                password: '',
                password_old: ''
            }
        },
        methods: {
            showChangePass: function () {
                $('#changePassMdl').modal('show');
                this.new_pass = {
                    password_confirmation: '',
                        password: '',
                        password_old: ''
                };
            },
            saveNewPass: function () {
                var $this = this;
                $.ajax({
                    url: 'users/update/pass',
                    method: 'GET',
                    data: $this.new_pass,
                    success: function (data) {
                        swal('Updated!', 'New password has been set.', 'success');
                        $('#changePassMdl').modal('hide');
                    },
                    error: function (data) {
                        var message = '';
                        $.each(data.responseJSON.errors, function (x, y) {
                            message += y[0] + '\n';
                        });
                        swal('Please try again', message, 'error');
                    }
                });
            }
        },
        mounted: function () {
        }
    });
</script>
</body>
</html>
