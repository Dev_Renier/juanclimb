@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-3">
                <div class="panel panel-default panel-login">
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group login-group">
                                <h2 class="login-title">ADMIN JUANCLIMB</h2>
                            </div>
                            <div class="form-group login-group">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-7 senpai">
                                    <input id="email" type="email" class="input-login" name="email"
                                           value="{{ old('email') }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-group login-group">
                                <label for="password" class="col-md-4 control-label">Password</label>
                                <div class="col-md-7 senpai">
                                    <input id="password" type="password" class="input-login" name="password" required>
                                </div>
                            </div>
                            <div class="form-group login-group">
                                <div class="col-md-12">
                                    <div class="btn-shadow-wrap">
                                        <button class="btn-login zen">Login</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
